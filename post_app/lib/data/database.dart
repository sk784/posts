import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {

  final String uid;

  DatabaseService({ this.uid});

  final CollectionReference postsCollection = Firestore.instance.collection(
      'posts');

  final CollectionReference commentsCollection = Firestore.instance.collection(
      'comments');


  Future<void> updatePostsData(String id, String userId, String title,
      String body) async {
    return await postsCollection.document(uid).setData({
      'id': id,
      'userId': userId,
      'title': title,
      'body': body,
    });
  }

  Future<void> updateCommentsData(String id, String postId, String name,
      String email, String body) async {
    return await commentsCollection.document(uid).setData({
      'id': id,
      'postId': postId,
      'name': name,
      'email': email,
      'body': body,
    });
  }
}