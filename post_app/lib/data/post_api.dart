import 'dart:convert';
import 'package:post_app/model/comment.dart';
import 'package:post_app/model/post.dart';
import 'package:http/http.dart' as http;

class PostApi {
  static const baseUrl = "https://jsonplaceholder.typicode.com";

  PostApi._privateConstructor();
  static final PostApi instance = PostApi._privateConstructor();

  Future<List<Post>> getPosts() async {
    final postsUrl = '$baseUrl/posts';

    final postsRequest = await http.get(postsUrl);

    if (postsRequest.statusCode != 200) {
      print(postsRequest.statusCode);
      throw Exception('Error!');
    }

    final postsJson = jsonDecode(postsRequest.body) as List;
    List<Post> posts =
    postsJson.map((post) => Post.fromJson(post)).toList();
    return posts;
  }

  Future<List<Comment>> getComments(int postId) async {
    final commentsUrl = '$baseUrl/posts/$postId/comments';
    final commentsRequest = await http.get(commentsUrl);

    if (commentsRequest.statusCode != 200) {
      print(commentsRequest.statusCode);
      throw Exception('Error!');
    }

    final commentsJson = jsonDecode(commentsRequest.body) as List;
    List<Comment> comments =
    commentsJson.map((comment) => Comment.fromJson(comment)).toList();
    return comments;
  }
}