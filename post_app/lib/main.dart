import 'package:flutter/material.dart';
import 'package:post_app/screens/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Posts App',
      home: Scaffold(
        body: HomePage(),
      ),
    );
  }
}

