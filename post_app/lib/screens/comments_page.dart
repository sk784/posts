import 'package:flutter/material.dart';
import 'package:post_app/data/database.dart';
import 'package:post_app/data/post_api.dart';
import 'package:post_app/model/comment.dart';

class CommentsPage extends StatefulWidget {
  const CommentsPage({Key key,this.postId}) : super(key: key);

  final int postId;

  @override
  _CommentsPageState createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {

  var service = PostApi.instance;
  Future<List<Comment>> getComments;
  final DatabaseService _databaseService = DatabaseService();

  @override
  void initState() {
    super.initState();
    getComments = service.getComments(widget.postId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Comments'),
      ),
      body: FutureBuilder<List<Comment>>(
        future: getComments,
        builder: (context, snapshot) {
          if (snapshot.data != null) {
            final comments = snapshot.data;
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                _addCommentsToDatabase(comments[index].id.toString(),comments[index].postId.toString(),
                    comments[index].name, comments[index].email, comments[index].body);
                return Card(
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: ListTile(
                      leading: Text(
                        comments[index].id.toString(),
                      ),
                      title: Text('${comments[index].name}\n\n ${comments[index].email}\n',),
                       subtitle: Text(comments[index].body),
                    ),
                  ),
                );
              },
            );
          } else if (snapshot.hasError) {
            return Padding(
              padding: const EdgeInsets.only(top: 16.0,left: 16.0),
              child: Text('Ошибка при загрузке данных: ${snapshot.error}'),
            );
          } else {
            return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                )
            );
          }
        },
      ),
    );
  }

  Future _addCommentsToDatabase(String id, String postId, String name, String email, String body) async {
    _databaseService.updateCommentsData(id,postId,name,email,body);
  }


}
