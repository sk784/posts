import 'package:flutter/material.dart';
import 'package:post_app/data/database.dart';
import 'package:post_app/data/post_api.dart';
import 'package:post_app/model/post.dart';

import 'comments_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var service = PostApi.instance;
  final DatabaseService _databaseService = DatabaseService();

  Future<List<Post>> getPosts;

  @override
  void initState() {
    super.initState();
    getPosts = service.getPosts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Posts'),
        ),
        body: FutureBuilder<List<Post>>(
              future: getPosts,
              builder: (context, snapshot) {
                if (snapshot.data != null) {
                  final posts = snapshot.data;
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      _addPostsToDatabase(posts[index].id.toString(),posts[index].userId.toString(),
                          posts[index].title, posts[index].body);
                      return GestureDetector(
                        onTap: () =>
                            Navigator.push(context,
                              MaterialPageRoute(builder: (context) =>
                                  CommentsPage(postId: index + 1)
                              ),
                            ),
                        child: Card(
                          child: Padding(
                            padding: EdgeInsets.all(16.0),
                            child: ListTile(
                              leading: Text(
                                posts[index].id.toString(),
                              ),
                              title: Text('${posts[index].title}\n',),
                              subtitle: Text(posts[index].body),
                            ),
                          ),
                        ),
                      );
                    },
                  );
                   } else if (snapshot.hasError) {
                 return Padding(
                   padding: const EdgeInsets.only(top: 16.0,left: 16.0),
                       child: Text('Ошибка при загрузке данных: ${snapshot.error}'),
                    );
                    } else {
                    return Container(
                       child: Center(
                            child: CircularProgressIndicator(),
                      )
                    );
                }
              },
            ),
    );
  }

  Future _addPostsToDatabase(String id, String userId, String title, String body) async {

    _databaseService.updatePostsData(id,userId,title,body);
  }
}



